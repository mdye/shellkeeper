(defproject shellkeeper "0.1.0-SNAPSHOT"
  :description "REST API for storing and retrieving shell commands, primarily BASH commands"
  :url "https://bitbucket.org/mdye/shellkeeper"
  :license {:name "GPL v3.0"
            :url "http://www.gnu.org/licenses/gpl.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [compojure "1.1.6"]
                 [ring/ring-json "0.2.0"]
                 [ring-server "0.3.0"]]
  :plugins [[lein-ring "0.8.7"]]
  :ring {:handler shellkeeper.handler/app
         :init shellkeeper.handler/init
         :destroy shellkeeper.handler/destroy}
  :aot :all
  :profiles
  {:production
   {:ring
    {:open-browser? false, :stacktraces? false, :auto-reload? false}}
   :dev
   {:dependencies [[ring-mock "0.1.5"] [ring/ring-devel "1.2.0"]]}})
