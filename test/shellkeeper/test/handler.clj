(ns shellkeeper.test.handler
  (:use clojure.test
        ring.mock.request
        shellkeeper.handler))

(deftest test-app
  (testing "main route"
    (let [response (app (request :get "/sk/0.1.0/cmds"))]
      (is (= (:status response) 200))
      (is (= (:body response) "okie dokie"))))

  (testing "not-found route"
    (let [response (app (request :get "/cmds"))]
      (is (= (:status response) 404)))))
