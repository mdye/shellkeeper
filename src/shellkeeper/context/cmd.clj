(ns shellkeeper.context.cmd)

(defn list-cmds [] '(1 2 3 4))

(defn get-cmd [id] {:id id :code "tar cvf /tmp.tar /tmp"})

(defn new-cmd [body] (str "created new cmd with data " body))

(defn delete-cmd [id] (str "deleted cmd with id " id))
