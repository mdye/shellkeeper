(ns shellkeeper.handler
  (:use ring.util.response)
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]
            [ring.middleware.json :as middleware]
            [shellkeeper.context.cmd :as cmd]))

(defn init []
  (println "shellkeeper is starting"))

(defn destroy []
  (println "shellkeeper is shutting down"))

(defroutes app-routes
  ; context for entire app
  (context "/sk/0.1.0" []
    (context "/cmd" []
      (GET "/" [] (response (cmd/list-cmds)))
      (GET "/:id" [id] (response (cmd/get-cmd id)))
      (POST "/" {body :body} (response (cmd/new-cmd body)))
      (DELETE "/:id" [id] (response (cmd/delete-cmd id)))))
      ; TODO: determine what proper response error is for something like DELETE http://localhost:9000/sk/0.1.0/cmd; add handling for it

  ; not-found handler for app, even outside of context
  ; TODO: determine if errors like this should be JSON and if so, wrap 'em all
  (route/not-found "not found"))

(def app
  (-> (handler/api app-routes)
      (middleware/wrap-json-body)
      (middleware/wrap-json-response)))
